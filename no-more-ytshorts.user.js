// ==UserScript==
// @name         youtube short interface remover
// @namespace    http://tampermonkey.net/
// @version      0.8
// @description  will replace the youtube short links with watch links
// @author       tweimann
// @match        https://www.youtube.com/*
// @icon         https://lh3.googleusercontent.com/8c7Dx3iLPs8LGfy7y9fieS1KPhTcHaPUX9hp0gchmr2z6x6qZdfzQ1oKw4I0gdFkBEw=w300
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    const currentLocation = String(window.location);
    
    if (currentLocation.startsWith('https://www.youtube.com/shorts/')) {
        var formattedLocation = currentLocation.replace('www.youtube.com/shorts/', 'www.youtube.com/watch?v=');
        window.open(formattedLocation, '_self');
        console.log('link fixed!')
    }
}());
