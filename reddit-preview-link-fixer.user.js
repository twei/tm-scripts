// ==UserScript==
// @name         reddit preview link fixer
// @namespace    http://tampermonkey.net/
// @version      0.9
// @description  will fix the reddit preview links so it doesn't show the .webp images
// @author       twei
// @match        https://www.reddit.com/media*
// @match        https://preview.redd.it/*
// @icon         https://www.google.com/s2/favicons?domain=reddit.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    console.log('script started');

    const currentLocation = String(window.location);

    if (currentLocation.includes('preview.redd.it')) {
        // if current location does include preview.redd.it, redirect to i.redd.it without the query parameters
        // this is for links like https://preview.redd.it/abcdefg.jpg?width=420&format=pjpg&auto=webp
        var fixedLocation = currentLocation
            .replace('preview.redd.it/', 'i.redd.it/')
            .replace(/\/.*-/, "/")
            .slice(0, currentLocation.indexOf('?'));

        console.log('new link: ' + fixedLocation);
        window.open(fixedLocation, '_self');
        console.log('link fixed!')

    } else if (currentLocation.includes('i.redd.it') == false) {
        // if current location does not include i.redd.it, stop the page from loading
        // this is for links like https://www.reddit.com/media?url=https%3A%2F%2Fpreview.redd.it%2Fabcdefg.jpg%3Fwidth%3D420%26format%3Dpjpg%26auto%3Dwebp
        window.stop();
        console.log('stopped page from loading');

    } else if (currentLocation.includes('i.redd.it')) {
        // if current location does include i.redd.it, redirect from reddit.com/media/
        // this too is for links like https://www.reddit.com/media?url=https%3A%2F%2Fpreview.redd.it%2Fabcdefg.jpg%3Fwidth%3D420%26format%3Dpjpg%26auto%3Dwebp
        // this will replace the link with something like https://www.reddit.com/media?url=https%3A%2F%2Fi.redd.it%2Fabcdefg.jpg
        var fixedLocation = currentLocation
            .replace('preview.redd.it', 'i.redd.it')
            .slice(0, currentLocation.indexOf('%3F') - 6) // remove query parameters after filename
            .replace(/(it%2F).*v0-/, "$1"); // remove post-name from image file name

        console.log('new link: ' + fixedLocation);
        window.open(fixedLocation, '_self');
        console.log('link fixed!');
    }

}());